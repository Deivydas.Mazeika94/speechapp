package com.example.speechtotext.models

data class Message(
    var message: String,
    var isReceived: Boolean
)